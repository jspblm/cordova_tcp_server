// Ionic Starter App
// https://github.com/MobileChromeApps/cordova-plugin-chrome-apps-sockets-tcp.git
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var tcpClient = null;
var socketId = null;
var server_ip = '';

function ab2str(buf) {
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}
function str2ab(str) {
  var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
  var bufView = new Uint16Array(buf);
  for (var i=0, strLen=str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}


angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
      tcpClient = chrome.sockets.tcp;
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.controller('mainCtrl', function($scope, $ionicPlatform){
  console.log('mainCtrl');

  $scope.data_sent = '1234567891234567';
  $scope.status_log = '';
  $scope.server_ip = '192.168.1.12';

  $scope.socket_create = function(){
    tcpClient.create({name: 'tcp_client'}, function(createInfo){
      socketId = createInfo.socketId;
      console.log('createInfo', createInfo);
      $scope.$apply(function(){
        $scope.status_log = 'create ' + createInfo;
      });
    });
  }

  $scope.set_ip = function(valor){
    console.log('valor', valor);
    server_ip = valor;
    console.log('set_ip', server_ip);
  }

  $scope.connect = function(){
    console.log('connecting', server_ip);
    tcpClient.connect(socketId, server_ip, 8000, function(connect_info){
      console.log('connect', connect_info);
      $scope.$apply(function(){
        $scope.status_log = 'connect ' + connect_info;
      });
    });
  }

  $scope.disconnect = function(){
    tcpClient.disconnect(socketId, function(info){
      console.log('disconnect', info);
    });
  }

  $scope.send = function(data_sent){
    // var data_sent = $scope.data_sent;
    data_sent_ab = str2ab(data_sent);
    tcpClient.send(socketId, data_sent_ab, function(sendInfo){
      console.log('send', sendInfo);
      $scope.$apply(function(){
        $scope.status_log = 'sent ' + sendInfo;
      });
    });
  }

  $scope.close = function(){
    tcpClient.close(socketId, function(info){
      console.log('close', info)
    });
  }

});
